import unittest
import datetime
from restaurant import Restaurant, ProductionLine, Workstation


class TestRestaurant(unittest.TestCase):
    def setUp(self) -> None:
        self.restaurant_info = {
            "restaurant_id": "R1",
            "cooking_capacity": 4.0,
            "cooking_time": 1.0,
            "assembly_capacity": 3.0,
            "assembly_time": 2.0,
            "packing_capacity": 2.0,
            "packing_time": 1.0,
            "burgerpatty_count": 100,
            "lettuce_count": 200,
            "tomato_count": 200,
            "veggiepatty_count": 100,
            "bacon_count": 100,
        }

    def test_initialisation(self):
        # Should not throw
        restaurant = Restaurant(self.restaurant_info)
        self.assertDictEqual(self.restaurant_info, restaurant.status())
        # Check that production line has been set up correctly
        self.assertDictEqual(
            restaurant.production_line.status(),
            {
                "cooking_capacity": 4.0,
                "cooking_time": datetime.timedelta(minutes=1),
                "assembly_capacity": 3.0,
                "assembly_time": datetime.timedelta(minutes=2),
                "packing_capacity": 2.0,
                "packing_time": datetime.timedelta(minutes=1),
            },
        )


class TestProductionLine(unittest.TestCase):
    def setUp(self) -> None:
        self.restaurant_info = {
            "restaurant_id": "R1",
            "cooking_capacity": 4.0,
            "cooking_time": 1.0,
            "assembly_capacity": 3.0,
            "assembly_time": 2.0,
            "packing_capacity": 2.0,
            "packing_time": 1.0,
            "burgerpatty_count": 100,
            "lettuce_count": 200,
            "tomato_count": 200,
            "veggiepatty_count": 100,
            "bacon_count": 100,
        }

    def test_initialisation(self):
        # Should not throw
        prod_line = ProductionLine(self.restaurant_info)
        self.assertDictEqual(
            prod_line.status(),
            {
                "cooking_capacity": 4.0,
                "cooking_time": datetime.timedelta(minutes=1),
                "assembly_capacity": 3.0,
                "assembly_time": datetime.timedelta(minutes=2),
                "packing_capacity": 2.0,
                "packing_time": datetime.timedelta(minutes=1),
            },
        )
        # Test that graph has been created correctly and can be transversed


class TestWorkstation(unittest.TestCase):
    def setUp(self) -> None:
        self.burger = {
            "restaurant_id": "R1",
            "timestamp": datetime.datetime(2020, 12, 8, 19, 15, 32),
            "order_id": "O2",
            "start_timestamp": None,
            "current_timestamp": None,
        }

        self.order = {
            "restaurant_id": "R1",
            "timestamp": datetime.datetime(2020, 12, 8, 19, 15, 32),
            "order_id": "O2",
            "orders": ["VLT", "VT", "BLT", "LT", "VLT"],
        }

    def test_initialisation(self):
        # Should not throw
        work_station = Workstation(
            type="Example", time_taken=1.0, capacity=3, next_workstation=None
        )

        self.assertFalse(work_station.is_full())

    def test_add_items(self):
        """Test adding multiple items, making sure they are added correctly to the
        in_progress_queue and then wait_queue
        """
        # Should not throw
        work_station = Workstation(
            type="Example", time_taken=1.0, capacity=3, next_workstation=None
        )

        self.assertFalse(work_station.is_full())

        work_station.add_item(
            {**self.burger, "burger": "VLT"},
            time_stamp=datetime.datetime(2020, 12, 8, 19, 15, 32),
        )
        self.assertFalse(work_station.is_full())
        work_station.add_item(
            {**self.burger, "burger": "BLT"},
            time_stamp=datetime.datetime(2020, 12, 8, 19, 15, 32),
        )
        self.assertFalse(work_station.is_full())
        work_station.add_item(
            {**self.burger, "burger": "LT"},
            time_stamp=datetime.datetime(2020, 12, 8, 19, 15, 32),
        )
        self.assertTrue(work_station.is_full())
        self.assertTrue(len(work_station.wait_queue) == 0)
        work_station.add_item(
            {**self.burger, "burger": "L"},
            time_stamp=datetime.datetime(2020, 12, 8, 19, 15, 32),
        )
        self.assertTrue(work_station.is_full())
        self.assertFalse(work_station.wait_queue.empty())
        self.assertEqual(work_station.wait_queue.qsize(), 1)
        work_station.add_item(
            {**self.burger, "burger": "T"},
            time_stamp=datetime.datetime(2020, 12, 8, 19, 15, 32),
        )
        self.assertEqual(work_station.wait_queue.qsize(), 2)
        self.assertEqual(work_station.wait_queue.get()[0]["burger"], "L")
        self.assertEqual(work_station.wait_queue.qsize(), 1)
        self.assertEqual(work_station.wait_queue.get()[0]["burger"], "T")
        self.assertTrue(work_station.wait_queue.empty())

        # Test that the next time tick has been updated correctly
        self.assertEqual(
            work_station.next_tick, datetime.datetime(2020, 12, 8, 19, 16, 32)
        )
    #
    # def test_yield_items(self):
    #     """Test moving items across a workstation"""
    #     work_station = Workstation(
    #         type="Example", time_taken=1.0, capacity=3, next_workstation=None
    #     )
    #     for burger in self.order["orders"]:
    #         work_station.add_item(
    #             {**self.burger, "burger": burger},
    #             time_stamp=datetime.datetime(2020, 12, 8, 19, 15, 32),
    #         )
    #
    #     completed_burgers = [
    #         x for x in work_station.yield_item(
    #             current_timestamp=datetime.datetime(2020, 12, 8, 19, 16, 32)
    #         )
    #     ]
    #     print(len(completed_burgers))
