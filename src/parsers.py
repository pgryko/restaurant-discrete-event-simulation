from dateutil import parser

"""
Parsers for extracting either
* restaurant capacity statistics
* order information

Note: should have used a library e.g.
https://pypi.org/project/csvvalidator/
or
https://pydantic-docs.helpmanual.io/,

also https://marshmallow.readthedocs.io/ might have been useful
"""


def parse_order_input(splitlist: str):
    """Parse and Validate that input order is of Type

    <RestaurantID>,<ISODateTime>,<OrderID>,<OrderType>,<OrderType>,<OrderType>,[Optional additional <OrderType>]

    RR1,2020-12-08 21:15:31,ORDER1,BLT,LT,VLT

    :param string:
    :return: dict: {'RESTAURANTID': restaurant,
            'TIMESTAMP': timestamp,
            'ORDERID': order_id,
            'ORDERS': orders}
    :return: raises ValueError or ParseError: if order malformed
    """
    # splitlist can also raise an exception

    # There should be at least 4 values, restaurant, datetime, orderid, order
    if len(splitlist) < 4:
        raise ValueError("Malformed input: Less than 4 parameters in order")

    # Allowed order types
    # B for Bacon
    # L for Lettuce
    # T for Tomatoes
    # V for Veggie burger
    allowed = {"B", "L", "T", "V"}
    # We are not checking for weird orders, e.g. bbbb (quad bacon), bbbvvv (triple bacon with triple veggie patties)
    # We also have no way to order a burger with no vegetables or bacon - suggestion add a new char P for 'Plain'

    restaurant_id = splitlist[0]
    # This may raise an exception, if datetime is not correctly formatted to iso standard.
    timestamp = parser.parse(splitlist[1])
    order_id = splitlist[2]
    orders = []

    for index in range(3, len(splitlist)):
        if set(splitlist[index]) <= allowed:
            # Handle the case where extra commas are added, to make sure empty orders are not created
            # e.g. "LT,VLT," should give ["LT","VLT"] and not ["LT","VLT",""]
            # if len(splitlist[index]) > 0:
            orders.append(splitlist[index])
        else:
            raise ValueError(
                "Malformed input: burger does not contain only chars B,L,T,V"
            )

    return {
        "restaurant_id": restaurant_id,
        "timestamp": timestamp,
        "order_id": order_id,
        "orders": orders,
    }


def parse_restaurant_stats(splitlist: str):
    """Parse restaurant capacity statistics, expect comma separate format of form

    <RestaurantID>,<CookingCapacity>,<CookingTime>,<AssemblyCapacity>,<AssemblyTime>,<PackingCapacity>,<PackingTime>
    ,<BurgerPattyCount>,<LettuceCount>,<TomatoCount>,<VeggiePattyCount>,<BaconCount>

    E.g.
    R1,4C,1,3A,2,2P,1,100,200,200,100,100

    :param restaurant_info:
    :return: dict { }
    :return: raises ValueError or ParseError: if order malformed
    """

    def _strip_end_char(capacity_string: str):
        # cooking_capacity, assembly_capacity, packing_capacity, come in an alphanumeric format
        # e.g. '4C', '3A', '2P',
        # Strip out the letters and cast to float
        # Should we get a malformed input, e.g. '2' or 'P' instead of '2P' let an exception to be raised
        # or raise one directly, and cast to float (explicit check for numeric type)

        # Possibility of raising exception
        capacity_str = capacity_string[:-1]
        if capacity_string == "":
            raise ValueError(
                "Malformed input: Expected [number][letter] format, e.g. 4A, instead got "
                + capacity_string
            )

        return float(capacity_str)

    # There should be at least 4 values, restaurant, datetime, orderid, order
    if len(splitlist) != 12:
        raise ValueError("Malformed input: Expected 4 parameters in restaurant info")

    # cooking_capacity, assembly_capacity, packing_capacity, come in an alphanumeric format
    # e.g. '4C', '3A', '2P',
    # use _strip_end_char to remove letter and explicitly check for numeric type by cast to float

    # TODO: check that capacity and time is not negative

    return {
        "restaurant_id": splitlist[0],
        "cooking_capacity": _strip_end_char(splitlist[1]),
        "cooking_time": float(splitlist[2]),
        "assembly_capacity": _strip_end_char(splitlist[3]),
        "assembly_time": float(splitlist[4]),
        "packing_capacity": _strip_end_char(splitlist[5]),
        "packing_time": float(splitlist[6]),
        "burgerpatty_count": int(splitlist[7]),
        "lettuce_count": int(splitlist[8]),
        "tomato_count": int(splitlist[9]),
        "veggiepatty_count": int(splitlist[10]),
        "bacon_count": int(splitlist[11]),
    }


def parse_input(input_string: str):
    """Parse input, higher order wrapper function
    Attempt to determine whether input string is of type restaurant info or order info.
    Attempt to extract either restaurant statistics or order details

    Attempt to handle and log all parse errors

    :param input:
    :return:
    """

    restaurant_stats = None
    order = None
    # Do nothing with the errors now, but useful to keep track of parse errors
    # Let a higher function control what to do with them - e.g. log them to rsyslog and logship them
    # or return malformed input to sender as part of response
    error_log = None

    try:
        # Test to make sure character set is ascii, to reduce surface attack area
        # i.e. system sending in unusual unicode chars.
        if not input_string.isascii():
            raise ValueError("Non ascii characters detected")
        # For good measure remove common chars used in sql injection. We should not expect them in the api call
        # There's probably a more performant/pythonic way to do this
        splitlist = (
            input_string.replace(";", "").replace("'", "").replace('"', "").split(",")
        )
        if len(splitlist) < 2:
            raise ValueError("Malformed input: Less than 2 parameters in csv string")
        # Check whether second element is alphanumeric or datetime type
        # to see if we are working with restaurant data or order data
        try:
            parser.parse(splitlist[1])
            # If no error raised, parsed correctly
            order = parse_order_input(splitlist)
        except parser.ParserError:
            restaurant_stats = parse_restaurant_stats(splitlist)

    except Exception as e:
        error_log = {
            "message": input_string,
            "error": str(e),
        }

    return restaurant_stats, order, error_log


def parse_inventory(orders: list):
    """Given a list of orders, count and parse the ingredients
    I.e. convert a list of orders into an inventory dict

    :param orders: List of orders, e.g. ['BLT', 'LT', 'VLT']
    :return: dict of format {'burgerpatty_count': int, 'lettuce_count': int,
                             'tomato_count': int, 'veggiepatty_count': int,
                             'bacon_count': int}
    """
    character_dict = {}

    for str_element in orders:
        for char in str_element:
            if char not in character_dict.keys():
                character_dict[char] = 1
            else:
                character_dict[char] += 1

    # Calculate burger_patty_count, this will be the difference between total orders and
    if "V" in character_dict.keys():
        used_ingredients = {"burgerpatty_count": len(orders) - character_dict["V"]}
    else:
        used_ingredients = {"burgerpatty_count": len(orders)}

    character_map = {
        "B": "bacon_count",
        "L": "lettuce_count",
        "T": "tomato_count",
        "V": "veggiepatty_count",
    }

    for char in character_dict:
        used_ingredients[character_map[char]] = character_dict[char]

    return used_ingredients
