import logging
from parsers import parse_input


class Runner:
    """Primary kernel for handling incoming orders & restaurant stats from api
    and returning accepted/rejected for orders
    """

    def __init__(self):
        # Use a dict for storing current restaurant state
        # If restaurant logic grows, refactor restaurant state into dedicated class
        self.restaurants_state = {}
        self.orders = []

    def handle_order_submission(self, order_strings: list[str]):
        """Parse orders at highest level one, by one and after each parse, attempt to
        either accept or reject order

        Expect a list of strings in either restaurant capacity or order id format

        E.g.

        R1,4C,1,3A,2,2P,1,100,200,200,100,100
        R1,2020-12-08 19:15:31,O1,BLT,LT,VLT
        R1,2020-12-08 19:15:32,O2,VLT,VT,BLT,LT,VLT
        R1,2020-12-08 19:16:05,O3,VLT,VT,BLT,LT,VLT
        R1,2020-12-08 19:17:15,O4,BT,BLT,VLT,BLT,BT,LT,VLT
        R1,2020-12-08 19:19:10,O5,BLT,LT,VLT
        R1,2020-12-08 19:15:32,O6,VLT,VT,BLT,VLT,BT
        R1,2020-12-08 19:16:05,O7,VLT,LT,BLT,LT,VLT
        R1,2020-12-08 19:17:15,O8,BT,BLT,VLT,BLT,BLT
        R1,2020-12-08 19:18:15,O9,BT,BLT,VLT,BLT,BLT
        R1,2020-12-08 19:21:10,O10,BLT,VLT
        R1,2020-12-08 19:25:17,O11,VT,VLT
        R1,2020-12-08 19:28:17,O12,VT,VLT

        Note for future work:

        We want to perform parsing in a serial manner here (i.e. one by one)
        rather than in a batch (or parallel process) due to planning for further requirements.

        E.g. say we want at a later date to handle on the fly changes in restaurant capacity
        (workers go on break, shifts change or food gets spoiled/dropped), e.g. handle something like

        R1,4C,1,3A,2,2P,1,100,200,200,100,100
        R1,2020-12-08 19:15:31,O1,BLT,LT,VLT
        R1,2020-12-08 19:15:32,O2,VLT,VT,BLT,LT,VLT
        R1,2020-12-08 19:16:05,O3,VLT,VT,BLT,LT,VLT
        R1,4C,1,1A,2,2P,1,100,200,200,100,100
        R1,2020-12-08 19:17:15,O4,BT,BLT,VLT,BLT,BT,LT,VLT
        """

        for order_string in order_strings:
            restaurant_stats, order, error_log = parse_input(order_string)

            if restaurant_stats:
                self.restaurants_state[
                    restaurant_stats["restaurant_id"]
                ] = restaurant_stats
            elif order:
                self.orders.append(order)
                # Todo: Handle order parsing
            if error_log:
                # For now do nothing but print to console
                # In prod, log to rsyslog or logship directly to a logging system
                logging.warning(error_log)


if __name__ == "__main__":
    print("Ran runner")
