import unittest
import datetime
from parsers import (
    parse_input,
    parse_restaurant_stats,
    parse_order_input,
    parse_inventory,
)


class TestParsers(unittest.TestCase):
    def setUp(self) -> None:
        self.input_list = [
            "R1,4C,1,3A,2,2P,1,100,200,200,100,100",
            "R1,2020-12-08 19:15:31,O1,BLT,LT,VLT",
            "R1,2020-12-08 19:15:32,O2,VLT,VT,BLT,LT,VLT",
            "R1,2020-12-08 19:16:05,O3,VLT,VT,BLT,LT,VLT",
            "R1,2020-12-08 19:17:15,O4,BT,BLT,VLT,BLT,BT,LT,VLT",
            "R1,2020-12-08 19:19:10,O5,BLT,LT,VLT",
            "R1,2020-12-08 19:15:32,O6,VLT,VT,BLT,VLT,BT",
            "R1,2020-12-08 19:16:05,O7,VLT,LT,BLT,LT,VLT",
            "R1,2020-12-08 19:17:15,O8,BT,BLT,VLT,BLT,BLT",
            "R1,2020-12-08 19:18:15,O9,BT,BLT,VLT,BLT,BLT",
            "R1,2020-12-08 19:21:10,O10,BLT,VLT",
            "R1,2020-12-08 19:25:17,O11,VT,VLT",
            "R1,2020-12-08 19:28:17,O12,VT,VLT",
        ]

        self.expected_orders = [
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 15, 31),
                "order_id": "O1",
                "orders": ["BLT", "LT", "VLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 15, 32),
                "order_id": "O2",
                "orders": ["VLT", "VT", "BLT", "LT", "VLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 16, 5),
                "order_id": "O3",
                "orders": ["VLT", "VT", "BLT", "LT", "VLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 17, 15),
                "order_id": "O4",
                "orders": ["BT", "BLT", "VLT", "BLT", "BT", "LT", "VLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 19, 10),
                "order_id": "O5",
                "orders": ["BLT", "LT", "VLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 15, 32),
                "order_id": "O6",
                "orders": ["VLT", "VT", "BLT", "VLT", "BT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 16, 5),
                "order_id": "O7",
                "orders": ["VLT", "LT", "BLT", "LT", "VLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 17, 15),
                "order_id": "O8",
                "orders": ["BT", "BLT", "VLT", "BLT", "BLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 18, 15),
                "order_id": "O9",
                "orders": ["BT", "BLT", "VLT", "BLT", "BLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 21, 10),
                "order_id": "O10",
                "orders": ["BLT", "VLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 25, 17),
                "order_id": "O11",
                "orders": ["VT", "VLT"],
            },
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 28, 17),
                "order_id": "O12",
                "orders": ["VT", "VLT"],
            },
        ]

        self.maxDiff = None

    def test_parse_valid_input(self):
        """Test that a correctly formatted bulk input is correctly parsed"""

        # Pass in string containing restaurant stat
        restaurant_stats, order, error_log = parse_input(self.input_list[0])
        self.assertDictEqual(
            restaurant_stats,
            {
                "restaurant_id": "R1",
                "cooking_capacity": 4.0,
                "cooking_time": 1.0,
                "assembly_capacity": 3.0,
                "assembly_time": 2.0,
                "packing_capacity": 2.0,
                "packing_time": 1.0,
                "burgerpatty_count": 100,
                "lettuce_count": 200,
                "tomato_count": 200,
                "veggiepatty_count": 100,
                "bacon_count": 100,
            },
        )

        # Pass in strings containing only order info
        for input_string, expected_order in zip(
            self.input_list[1:], self.expected_orders
        ):
            restaurant_stats, order, error_log = parse_input(input_string)
            self.assertDictEqual(order, expected_order)
            # Expect no errors
            self.assertIsNone(error_log)
            # Expect no restaurant stats
            self.assertIsNone(restaurant_stats)

    def test_parse_invalid_input(self):
        """Test that incorrectly formatted input is handled correctly"""
        # Test rejection of non ascii characters
        restaurant_stats, order, error_log = parse_input(
            "R1,4C,1,3A,2,2P,1,100,200,200,100,100,ф"
        )
        self.assertDictEqual(
            error_log,
            {
                "message": "R1,4C,1,3A,2,2P,1,100,200,200,100,100,ф",
                "error": "Non ascii characters detected",
            },
        )
        # Test striping of escape chars
        restaurant_stats, order, error_log = parse_input(
            "R1,2020-12-08 19:15:32,';\"O2,VLT,VT,BLT,LT,VLT"
        )
        self.assertDictEqual(
            order,
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 15, 32),
                "order_id": "O2",
                "orders": ["VLT", "VT", "BLT", "LT", "VLT"],
            },
        )
        self.assertIsNone(error_log)

    def test_parse_valid_restaurant_stats(self):
        """Test that a correctly formatted restaurant info string is correctly parsed"""
        restaurant_stats = parse_restaurant_stats(self.input_list[0].split(","))
        self.assertEqual(
            restaurant_stats,
            {
                "restaurant_id": "R1",
                "cooking_capacity": 4.0,
                "cooking_time": 1.0,
                "assembly_capacity": 3.0,
                "assembly_time": 2.0,
                "packing_capacity": 2.0,
                "packing_time": 1.0,
                "burgerpatty_count": 100,
                "lettuce_count": 200,
                "tomato_count": 200,
                "veggiepatty_count": 100,
                "bacon_count": 100,
            },
        )

    def test_parse_valid_order(self):
        """Test that a correctly formatted restaurant info string is correctly parsed"""
        order_input = parse_order_input(self.input_list[1].split(","))
        self.assertEqual(
            order_input,
            {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 15, 31),
                "order_id": "O1",
                "orders": ["BLT", "LT", "VLT"],
            },
        )

    def test_parse_malformed_order(self):
        """Test that a malformed order raises an exception"""

        with self.assertRaises(Exception) as context:
            parse_order_input(self.input_list[0].split(","))

        self.assertTrue("Unknown string format" in str(context.exception))

        with self.assertRaises(Exception) as context:
            # Handle case where un-needed comma is added at the end
            parse_order_input("R1,2020-12-08 19:15:31,O1,BLT,LT,VLT,")

        self.assertTrue("Malformed input" in str(context.exception))

    def test_parse_inventory(self):
        """Test parser for converting a list of orders into an inventory dict"""

        # Test general case
        self.assertDictEqual(
            parse_inventory(["VLT", "VT", "BLT", "LT", "VLT"]),
            {
                "burgerpatty_count": 2,
                "veggiepatty_count": 3,
                "lettuce_count": 4,
                "tomato_count": 5,
                "bacon_count": 1,
            },
        )
        # Test only veggie
        self.assertDictEqual(
            parse_inventory(["VLT", "VT", "VLT", "VLT", "VLT"]),
            {
                "burgerpatty_count": 0,
                "veggiepatty_count": 5,
                "lettuce_count": 4,
                "tomato_count": 5,
            },
        )
        # Test only non veggie
        self.assertDictEqual(
            parse_inventory(["LT", "T", "BLT", "LT", "LT"]),
            {
                "burgerpatty_count": 5,
                "lettuce_count": 4,
                "tomato_count": 5,
                "bacon_count": 1,
            },
        )
        # Test passing in empty dict
        self.assertDictEqual(parse_inventory([]), {"burgerpatty_count": 0})
