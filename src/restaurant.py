"""
Logic for handing restaurant capacity and assigning/rejecting orders
"""
from __future__ import annotations
import datetime
from collections import deque
from copy import deepcopy
from typing import Optional

from parsers import parse_inventory

# https://en.wikipedia.org/wiki/M/M/1_queue
# https://simpy.readthedocs.io/en/latest/simpy_intro/index.html
class Workstation:
    """Class describing the behaviour of a workstation"""

    def __init__(
        self,
        type: str,
        time_taken: float,
        capacity: int,
        next_workstation: Optional[Workstation],
    ):
        # Type of workstation, e.g. cooking, assembly packing
        self.type = type
        self.capacity = capacity
        # time taken to process (e.g. cook
        self.time_taken = datetime.timedelta(minutes=time_taken)
        # queue module is treadsafe, commonly used in multi treading
        self.in_progress_queue = deque()
        self.wait_queue = deque()
        # Next time for work queue to move forward
        self.next_tick = None
        self.next_workstation = next_workstation

    def add_item(self, burger, time_stamp):
        """Either enter an item into the workstation or place it in a queue to wait
        for completion
        """
        if self.capacity > len(self.in_progress_queue):
            self.in_progress_queue.appendleft(
                (
                    {
                        **burger,
                        "burger_current_timestamp": time_stamp,
                    }
                )
            )
            self.next_tick = time_stamp + self.time_taken
        else:
            self.wait_queue.appendleft((burger, time_stamp))

    def yield_item(self, current_timestamp):
        """Remove and return the next item in the in_progress_queue
        Make sure that anything in the wait queue is moved into the in_process_queue

        Yields either the finished burger dict or None (when finished)

        """

        while (
            # While que is not empty
              len(self.in_progress_queue) > 0
            and current_timestamp >= self.in_progress_queue[0]["burger_current_timestamp"] + self.time_taken
        ):

            # Remove the ready burger (yummy!)
            finished_burger = self.in_progress_queue.pop()

            if len(self.wait_queue) > 0:
                waiting_burger = self.wait_queue.pop()

                self.in_progress_queue.appendleft(
                    (
                        {
                            **waiting_burger,
                            "burger_current_timestamp": current_timestamp,
                        }
                    )
                )
                self.next_tick = current_timestamp + self.time_taken

            yield finished_burger

    def is_full(self):
        return len(self.in_progress_queue) == self.capacity


class ProductionLine:
    """Class describing the Production line

    Example visualisation for
    R1,4C,1,3A,2,2P,1,100,200,200,100,100

    inbound order: BLT,LT,VLT

    cooking (1min) (4c) -> assembly (2min) (3A) -> packing (1min) (2P)
    [BLT] -> 1(min)     -> [BLT] -> (2min)      -> [BLT] -> (1min) -> [VLT] -> (1 min)
    [LT]  -> 1(min)     -> [LT]  -> (2min)      -> [LT] -> (1min)  -> []
    [VLT] -> 1(min)     -> [VLT] -> (2min)
    []

    Total time 1 + 2 + 1 + 1 (from wait at packing) = 5min

    """

    def __init__(self, restaurant_info):

        self.packing = Workstation(
            type="packing",
            time_taken=restaurant_info["packing_time"],
            capacity=restaurant_info["packing_capacity"],
            next_workstation=None,
        )

        self.assembly = Workstation(
            type="assembly",
            time_taken=restaurant_info["assembly_time"],
            capacity=restaurant_info["assembly_capacity"],
            next_workstation=self.packing,
        )
        self.cooking = Workstation(
            type="cooking",
            time_taken=restaurant_info["cooking_time"],
            capacity=restaurant_info["cooking_capacity"],
            next_workstation=self.assembly,
        )

        # Entry point for graph
        self.start = self.cooking

    def status(self):
        return {
            "cooking_capacity": self.cooking.capacity,
            "cooking_time": self.cooking.time_taken,
            "assembly_capacity": self.assembly.capacity,
            "assembly_time": self.assembly.time_taken,
            "packing_capacity": self.packing.capacity,
            "packing_time": self.packing.time_taken,
        }

    def add_order(self, order):
        """

        :param order: {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 15, 32),
                "order_id": "O2",
                "orders": ["VLT", "VT", "BLT", "LT", "VLT"],
            }
        :return:
        """

        for burger in order["orders"]:
            burger_dict = deepcopy(order)
            burger_dict.pop("orders")
            burger_dict["burger"] = burger
            burger_dict["start_timestamp"] = burger_dict["start_timestamp"]
            burger_dict["burger_current_timestamp"] = burger_dict["start_timestamp"]
            self.cooking.add_item(burger_dict, order["timestamp"])

    def tick(self):
        """
        Move the Production line forward by the smallest tick
        :return:
        """
        # Sort the workstations by the smallest next tick, pushing any None values to end
        workstations = [self.cooking, self.assembly, self.packing].sort(
            key=lambda x: (x.next_tick is None, x.next_tick)
        )

        if workstations[0].next_tick:
            pass


class Restaurant:
    """Class for keeping track of restaurant statistics and order queue"""

    def __init__(self, restaurant_info: dict):
        """Initialise a restaurant from dict e.g.

        restaurant_info =           {'restaurant_id': 'R1', 'cooking_capacity': 4.0, 'cooking_time': 1.0,
                                     'assembly_capacity': 3.0, 'assembly_time': 2.0, 'packing_capacity': 2.0,
                                     'packing_time': 1.0, 'burgerpatty_count': 100, 'lettuce_count': 200,
                                     'tomato_count': 200, 'veggiepatty_count': 100, 'bacon_count': 100}

        :param restaurant_info dict: {'restaurant_id': str,
                                      'cooking_capacity': float,
                                      'cooking_time': float,
                                      'assembly_capacity': float,
                                      'assembly_time': float,
                                      'packing_capacity': float,
                                      'packing_time': float,
                                      'burgerpatty_count': int,
                                      'lettuce_count': int,
                                      'tomato_count': int,
                                      'veggiepatty_count': int,
                                      'bacon_count': int}

        """

        self.restaurant_info = restaurant_info
        self.production_line = ProductionLine(restaurant_info)

    def status(self):
        """Return ingredient status and production line capacities

        TODO: additionally return info about whats in ProductionLine queue
        :return: dict {'restaurant_id': str,
                                      'cooking_capacity': float,
                                      'cooking_time': float,
                                      'assembly_capacity': float,
                                      'assembly_time': float,
                                      'packing_capacity': float,
                                      'packing_time': float,
                                      'burgerpatty_count': int,
                                      'lettuce_count': int,
                                      'tomato_count': int,
                                      'veggiepatty_count': int,
                                      'bacon_count': int}
        """
        return {**self.restaurant_info}

    def _simulate_line(self, production_line, order) -> (bool, datetime):
        """Given an order, simulate how long it would take to cross the production line

        return True/False representing pass/fail
        """

        production_line.add_order(order)
        production_line.tick()

        accepted = True
        time_taken = datetime.timedelta(minutes=0)
        return accepted, time_taken

    def _handle_order_submission(self, order):
        """Accept a submitted order and simulate its progress through a restaurant AssemblyLine

        If the restaurant has enough components and assemble will happen in less than 20min, save assembly state
        and update restaurant info (ingredients)

        :param order dict: {
                "restaurant_id": "R1",
                "timestamp": datetime.datetime(2020, 12, 8, 19, 15, 32),
                "order_id": "O2",
                "orders": ["VLT", "VT", "BLT", "LT", "VLT"],
            }

        :return string:
        :return datetime.timedelta: time taken to process order, return a time of zero if order is rejected
        """

        # clone assembly_line object, if order is accepted, set the assembly_line object to be
        # current assembly_line
        # state (order accepted), otherwise discard the object (order rejected)

        production_line_simulation = deepcopy(self.production_line)

        accepted, time_taken = self._simulate_line(production_line_simulation, order)
        if accepted:
            # Update state
            self.assembly_line = production_line_simulation
            # Calculate change in ingredients and update self.restaurant_info
            used_ingredients = parse_inventory(order)
            for ingredients in used_ingredients:
                self.restaurant_info[ingredients] -= used_ingredients[ingredients]

            return (
                f"{self.restaurant_info['restaurant_id']},"
                f",Accepted,{time_taken.seconds / 60}",
                time_taken,
                accepted,
            )
        else:
            # Discard the assembly_line_simulation
            # Note: return time_taken as zero
            return (
                f"{self.restaurant_info['restaurant_id']}," f",Rejected",
                datetime.timedelta(minutes=0),
                accepted,
            )

    def process_bulk_order(self, orders: list[dict]) -> str:
        """ "Takes a list of orders, works through them in a linear fashion and checks whether they can
        be processes in 20min or less.

        Returns a formatted string containing order info and restaurant inventory

        Wrapper around _handle_order_submission, for processing orders passed in as bulk

        :param orders: list[dict] of format [
            {'restaurant_id': 'R1', 'timestamp': datetime.datetime(2020, 12, 8, 19, 15, 31), 'order_id': 'O1',
             'orders': ['BLT', 'LT', 'VLT']},
            {'restaurant_id': 'R1', 'timestamp': datetime.datetime(2020, 12, 8, 19, 15, 32), 'order_id': 'O2',
             'orders': ['VLT', 'VT', 'BLT', 'LT', 'VLT']}]

        :return string:
        '<Restaurant ID>,<Order ID>,<Accepted or Rejected>,<expected processing time in minutes>
        <Restaurant ID>,TOTAL,<Total time it takes to process all orders>
        <Restaurant ID>,Inventory,<Patties inventory>,<Lettuce inventory>,<Tomato inventory>,<Veggie patties inventory>,<Bacon Inventory>'

        """
        # Note, string concatenation is quadratic on complexity as strings are immutable
        # hence store as a list and then later join the response
        response = []
        total_processing_time = datetime.timedelta(minutes=0)

        for order in orders:
            (
                order_response_str,
                processing_time,
                accepted,
            ) = self._handle_order_submission(order)
            response.append(order_response_str + "\n")
            if accepted:
                total_processing_time += processing_time

        response.append(
            f"{self.restaurant_info['restaurant_id']},"
            f"TOTAL,"
            f"{total_processing_time}"
        )

        # Note: I would strongly prefer to return a json formatted string with key,values
        # to prevent arbitrary hardcoding of inventory order in response (and subsequent risk of
        # parse errors in more complicated systems)
        response.append(
            f"{self.restaurant_info['restaurant_id']},"
            f"Inventory,"
            f"{self.restaurant_info['burgerpatty_count']},"
            f"{self.restaurant_info['lettuce_count']},"
            f"{self.restaurant_info['tomato_count']},"
            f"{self.restaurant_info['veggiepatty_count']},"
            f"{self.restaurant_info['bacon_count']}"
        )

        return "".join(response)
