# Problem
A multi branch burger joint is trying to find a way to optimise the cooking process to maximise its profitability and reduce the cooking time. 
The joint takes orders online and each order comes as a new line of strings formatted in a particular format. 
This burger joint has different branches and each branch comes with a different capacity for cooking, assembling and packaging food along with the time it takes to process. Each restaurants’ capacity is also described in a new line of strings formatted in a particular format.
Each branch has an inventory to process burgers and usage of each material reduces inventory count by 1.

Find the most optimum way you can for processing orders. 

Make sure each order is processed within 20 minutes or less or reject the order if the kitchen can’t process it within 20 minutes. Each order could also get rejected if the branch runs out of inventory.

# Order Input
Each order comes as a new line in comma separated format. Each line includes:
Restaurant ID
Date and time of order in “YYYY-MM-DD hh:mm:ss” format
Order ID
A series of orders separated by comma with the following symbols:
B for Bacon
L for Lettuce
T for Tomatoes
V for Veggie burger

# Example Order Input:
Here is an example of an order and explanation

R1,2020-12-08 21:15:31,ORDER1,BLT,LT,VLT

# Restaurant ID is R1
Date and time of order is: 2020-12-08 21:15:31
Order ID is ORDER1
Order includes the following food to be cooked:
A burger with Bacon, Lettuce and Tomato
A burger with Lettuce and Tomato
A veggie burger with Lettuce and Tomato

Branch capacity and inventory input
Each branch has different capacity for cooking, assembling and packaging also has inventory for burger, bacon, lettuce, tomato and veggie burger.
This information will be presented before any orders on the first lines of the input data.

R1,4C,1,3A,2,2P,1,100,200,200,100,100

Restaurant with ID (R1) has the following structure:
Restaurant ID
Capacity for Cooking 4 burgers (including veggie)
It takes 1 minute to cook each burger
Capacity for Assembling 3 burgers
It takes 2 minutes to assemble each burger
Capacity to package 2 burgers
It takes 1 minute to package each burger 
Has burger patties inventory for 100 orders
Has lettuce inventory for 200 orders
Has tomato inventory for 200 orders
Has veggie patties inventory for 100 orders
Has bacon inventory for 100 orders

# Output
Output the ID of orders that can be processed along with the time it takes to process them and the ones being rejected because the branch is at full capacity. 
Print the total time it takes to process all orders for the branch.
Print the state of inventory

output should be formatted as: 
<Restaurant ID>,<Order ID>,<Accepted or Rejected>,<expected processing time in minutes>
<Restaurant ID>,TOTAL,<Total time it takes to process all orders>
<Restaurant ID>,Inventory,<Patties inventory>,<Lettuce inventory>,<Tomato inventory>,<Veggie patties inventory>,<Bacon Inventory>
R1,INVENTORY,58,130,115,63,50

For examples for 3 orders, output could look like this: (processing times are made up)
R1,O1,ACCEPTED,8
R1,O2,ACCEPTED,7
R1,O3,REJECTED
R1,TOTAL,25
R1,INVENTORY,58,130,115,63,50



# Test input
R1,4C,1,3A,2,2P,1,100,200,200,100,100
R1,2020-12-08 19:15:31,O1,BLT,LT,VLT
R1,2020-12-08 19:15:32,O2,VLT,VT,BLT,LT,VLT
R1,2020-12-08 19:16:05,O3,VLT,VT,BLT,LT,VLT
R1,2020-12-08 19:17:15,O4,BT,BLT,VLT,BLT,BT,LT,VLT
R1,2020-12-08 19:19:10,O5,BLT,LT,VLT
R1,2020-12-08 19:15:32,O6,VLT,VT,BLT,VLT,BT
R1,2020-12-08 19:16:05,O7,VLT,LT,BLT,LT,VLT
R1,2020-12-08 19:17:15,O8,BT,BLT,VLT,BLT,BLT
R1,2020-12-08 19:18:15,O9,BT,BLT,VLT,BLT,BLT
R1,2020-12-08 19:21:10,O10,BLT,VLT
R1,2020-12-08 19:25:17,O11,VT,VLT
R1,2020-12-08 19:28:17,O12,VT,VLT


Submission
Please zip your project directory including your .git folder and email it back to us
